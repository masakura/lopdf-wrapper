use pdf::node::Node;
use pdf::Pdf;

pub struct Page<'a> {
    node: Node<'a>,
}

impl<'a> Page<'a> {
    pub fn new(node: Node) -> Page {
        Page { node }
    }

    pub fn set_page(&mut self, num: u32) {
        self.node.set_string("PageNumber", num.to_string());
    }
}
