use lopdf::*;
use pdf::Font;
use pdf::Page;
use std::path::Path;

pub struct Pdf {
    doc: Document,
}

impl Pdf {
    fn new(doc: Document) -> Pdf {
        Pdf { doc }
    }

    pub fn load<P: AsRef<Path>>(path: P) -> Pdf {
        let doc = Document::load("").unwrap();
        Pdf::new(doc)
    }

    fn get_node(&self, id: ObjectId) -> Node {
        Node::new(self, id)
    }

    fn get_node_mut(&mut self, id: ObjectId) -> Node {
        Node::new_mut(self, id)
    }

    pub fn get_page(&self, id: ObjectId) -> Page {
        let node = self.get_node(id);
        Page::new(node)
    }

    pub fn get_page_mut(&mut self, id: ObjectId) -> Page {
        let node = self.get_node_mut(id);
        Page::new(node)
    }

    pub fn get_pages(&self) -> Vec<Page> {
        self.doc
            .get_pages()
            .iter()
            .map(|(_, id)| Page::new(self.get_node(*id)))
            .collect()
    }

    pub fn get_fonts(&self) -> Vec<Font> {
        self.doc
            .objects
            .iter()
            .filter_map(|(id, _)| Font::from_node(self.get_node(*id)))
            .collect()
    }
}

enum PdfContext<'a> {
    Ref(&'a Pdf),
    Mut(&'a mut Pdf),
}

enum ObjectContext<'a> {
    Ref(&'a Object),
    Mut,
}

pub struct Node<'a> {
    pdf: PdfContext<'a>,
    id: ObjectId,
    obj: ObjectContext<'a>,
}

impl<'a> Node<'a> {
    pub fn new(pdf: &Pdf, id: ObjectId) -> Node {
        Node {
            pdf: PdfContext::Ref(pdf),
            id,
            obj: ObjectContext::Ref(pdf.doc.get_object(id).unwrap()),
        }
    }

    pub fn new_mut(pdf: &mut Pdf, id: ObjectId) -> Node {
        Node {
            pdf: PdfContext::Mut(pdf),
            id,
            obj: ObjectContext::Mut,
        }
    }

    fn object(&self) -> &Object {
        match &self.obj {
            ObjectContext::Ref(obj) => obj,
            ObjectContext::Mut => match &self.pdf {
                PdfContext::Ref(pdf) => pdf.doc.get_object(self.id).unwrap(),
                PdfContext::Mut(pdf) => pdf.doc.get_object(self.id).unwrap(),
            },
        }
    }

    fn object_mut(&mut self) -> &mut Object {
        match &mut self.pdf {
            PdfContext::Ref(_) => panic!(""),
            PdfContext::Mut(pdf) => pdf.doc.get_object_mut(self.id).unwrap(),
        }
    }

    pub fn type_name(&self) -> &str {
        self.object().type_name().unwrap()
    }

    pub fn set_type_name(&mut self, name: &str) {
        let dict = self.object_mut().as_dict_mut().unwrap();
        dict.set("TypeName", name);
    }

    pub fn set_string(&mut self, key: &str, value: String) {
        let dict = self.object_mut().as_dict_mut().unwrap();
        dict.set(key, value);
    }
}
