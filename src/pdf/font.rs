use pdf::node::Node;

pub struct Font<'a> {
    node: Node<'a>
}

impl<'a> Font<'a> {
    pub fn from_node(node: Node) -> Option<Font> {
        match node.type_name() {
            "Font" => Some(Font { node }),
            _ => None,
        }
    }
}