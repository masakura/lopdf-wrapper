extern crate lopdf;

mod pdf;

use pdf::*;

fn main() {
    let mut pdf = Pdf::load("");
    {
        let mut page = pdf.get_page_mut((0, 0));
        page.set_page(15);
    }
    {
        pdf.get_fonts();
    }
}

